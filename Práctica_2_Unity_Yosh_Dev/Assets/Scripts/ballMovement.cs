﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ballMovement : MonoBehaviour {
    public float speed=40f;
    public Rigidbody rig;
    private Vector3 dir;
    private bool changeMovement;
    public Vector3 initialPosition;

    // Use this for initialization
    void Start () {
        initialPosition = transform.position;
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetMouseButtonDown(0))
        { 
            //Toma decision Direccion
            if (changeMovement == true)
            {
                //left is X
                dir = Vector3.left;
                changeMovement = false;
            }
            else
            {
                //forward is Z
                dir = Vector3.forward;
                changeMovement = true;
            }
        }
        //Calculo y ejecucion Direcccion
        transform.Translate(dir * speed * Time.deltaTime);
    }
}
