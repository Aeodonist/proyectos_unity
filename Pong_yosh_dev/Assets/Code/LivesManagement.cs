﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

/* This Script is use to do the LiveManagement of the Game to show
 * in a Graphical way the number of lives that the Player have.
 * And that when a life is lost the loss is shown and the total 
 * life counter is modified.
 */
public class LivesManagement : MonoBehaviour {
    // With this two variables (String,Int) I build the message that its going
    // to be displat in the Canvas
    public string liveText="Lives: ";
    public int totalLives=3;

    // We create this to Text Objects and in Unity we assign the Text Object
    // to be able to modify them and the text inside of it with C#
    public Text lives;

    // I create this two GameObjects and by Unity i associated the Ball and the
    // Shovel to it to access to their information, including the scripts associated
    // to that GameObject
    public GameObject myBall;
    public GameObject myShovel;

    // I also create this two Object to be able to contain the information of
    // the ball and shovel scripts
    private BallMovement ballMovement;
    private ShovelMovement shovelMovement;

    //GameObject that is going to allow me to .SetActive de GameOver Text
    public GameObject gameOver;

    // Use this for initialization
    /* At the start we put in the Object Text the combination of LiveText and
     * totalLives to display the message that indicates that how many lives 
     * left the Player has. And also to the Objects that we create to link the
     * Ball and Shovel Scripts to this one we assign the value of the execution
     * of the function GetComponent in the GameObject. 
     * To be able to later execute the Ball and the Shovel Reset functions. 
     */
    void Start () {
        lives.text = liveText+totalLives;
        ballMovement = myBall.GetComponent<BallMovement>();
        shovelMovement = myShovel.GetComponent<ShovelMovement>();
    }

    // Update is called once per frame
    /* In this Function we look if ata any moment the totalLives is 0
     * And when that happens, the GameOver text appears, and I change the values
     * for the speed of the Shovel and the Ball to 0, so in that way they are not going
     * to move, and finally with the Invoke function que execute the LoadCover function
     * 3 seconds after all the actions that have just been executed.
     */
    void Update () {
        if (totalLives==0) {
            gameOver.SetActive(true);
            shovelMovement.speed = 0f;
            ballMovement.initialSpeed = 0f;
            Invoke("LoadCover", 3);    
        }
    }

    //This Function is use to Load the Cover Scene
    void LoadCover()
    {
        SceneManager.LoadScene("pong_yosh_dev_cover");
    }

    /* Method to remove a life from the total of lives
     * And display the Message on the Object Text that its on the Canvas
     * And Reset the Ball and Shovel to their initialPositions
     * And also go back to making the ball daughter of the shovel
     */
    public void SubstractLife()
    {
        totalLives--;
        lives.text = liveText + totalLives;
        ballMovement.Reset();
        shovelMovement.Reset();
        ballMovement.transform.SetParent(shovelMovement.transform);
    }
}
