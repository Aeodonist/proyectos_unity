﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* This Script is to Manage all the Logic on the Plane to be able
 * to detect a Collision on the Plane.
 */
public class PlaneLogic : MonoBehaviour {
    // Boolean variable in which we will store if a collision has occurred or not
    public bool collision;
    // GameObject in which through Unity we passed the empty object associated with the LiveManagement Script.
    public GameObject gameManagement;

    // livesManagement object to store the associated Script of the GameObject
    private LivesManagement livesManagement;

    // Use this for initialization
    // and we assign to the LivesManagement object the GetComponent value of
    // the GameObject that we have associated to the Script through Unity
    void Start () {
        livesManagement = gameManagement.GetComponent<LivesManagement>();
    }

    // With this method we detect if any Collider of any GameObject with 
    // a RigidBody assign to it cross the Plane, if that happens this function 
    // executes and the boolean collision changes its value to true
    void OnTriggerEnter(Collider other)
    {
        collision = true;
    }

    // Update is called once per frame
    // Here we evaluate if a collision occurs if that happens, we put the value
    // of collison to false again and execute the funcion of Subtracting a Life
    void Update () {
        if (collision==true) {
            collision = false;
            livesManagement.SubstractLife();  
        }
	}
}
